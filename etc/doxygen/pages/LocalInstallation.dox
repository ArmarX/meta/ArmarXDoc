/**

\page ArmarXCore-Local-Installation Local ArmarX Installation

If it is not possible for you to install ArmarX with apt-get into /usr,
it is also possible to install ArmarX locally into your home directory.
\note This requires that all ArmarX dependencies are already installed (see \ref ArmarXDoc-Dependecies-Installation-standard-deb "here")!

Copy & paste the following lines into a file called *armarx-local-install.sh*:

\verbatim
#!/bin/bash
# cancel script on any error
set -e

installDir=$1
if [[ -z "${installDir// }" ]]
then
        installDir=$HOME/install
fi
echo "Installing into $installDir"
mkdir -p $installDir
tmpDir="/tmp/$USER-armarxdeb"
oldDir=`pwd`
mkdir -p "$tmpDir"
cd "$tmpDir"

packages=("armarx-armarxcore" "armarx-armarxgui" "armarx-robotapi"  "armarx-memoryx"  "armarx-visionx"  "python-armarx"  "armarx-armarxsimulation"  "armarx-robotcomponents"  "armarx-robotskilltemplates" "armarx-spoac"  "armarx-speechx"  "armarx-armar3" "simox" "ivt" "ivtrecognition" "ivtarmar3")
for package in "${packages[@]}"
do
  echo "Downloading package $package"
  apt-get download $package
done
export installDir
find "$tmpDir" -name '*armarx*.deb' -exec bash -c 'for file; do dpkg -x "$file" "$installDir" ; done' -- {} +
find "$tmpDir" -name '*armarx*.deb' -delete



if [[ $PATH == *$installDir* ]]
then
  echo "Env vars already adjusted";
else
  export PATH=$installDir/usr/bin:$installDir/usr/local/bin:$PATH
  export LD_LIBRARY_PATH=$installDir/usr/lib:$installDir/usr/local/lib:$LD_LIBRARY_PATH
  export PYTHONPATH=$installDir/usr/lib/python2.7/dist-packages:$PYTHONPATH
 
  echo "export ArmarXInstallDir=$installDir" >> ~/.bashrc
  echo "export PATH=\$ArmarXInstallDir/usr/bin:\$ArmarXInstallDir/usr/local/bin:\$PATH" >> ~/.bashrc
  echo "export LD_LIBRARY_PATH=\$ArmarXInstallDir/usr/lib:\$ArmarXInstallDir/usr/local/lib:\$LD_LIBRARY_PATH" >> ~/.bashrc
  echo "export PYTHONPATH=\$ArmarXInstallDir/usr/lib/python2.7/dist-packages:\$PYTHONPATH" >> ~/.bashrc
  echo "Added path variables to ~/.bashrc"
fi
echo "ArmarX installed locally - you can now start the ArmarX gui with: armarx gui"
cd $oldDir
set +e
\endverbatim

Now execute:

\code
source armarx-local-install.sh
\endcode

This will install ArmarX into $HOME/install, export needed environment variables and also add their export into the .bashrc.
If you want to install ArmarX into another directory, you can append the path to the command above.

*/
