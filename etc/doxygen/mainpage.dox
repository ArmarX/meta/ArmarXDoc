/**
  \mainpage ArmarX Documentation

  <center>
  <table class="doxtable" >
    <tr>
      <td style="border:0px"><center>\image html "StatechartEditor_thumbnail.png"
        \ref ArmarXGui-GuiPlugins-StatechartEditorController "The Statechart Editor"</center></td>
      <td style="border:0px"><center>\image html "Simulator_thumbnail.png"
        \ref ArmarXSimulation-Overview</center></td>
      <td style="border:0px"><center>\image html "affordances_office_pointcloud_cut_thumbnail.png"
        \ref VisionX-Overview "VisionX"</center></td>
    </tr>
    </table>
  </center>

  \section armarx-main-description Introduction

  The robot software development environment %ArmarX aims at providing an infrastructure for developing a customized robot framework that enables the realization of distributed robot software components.
  To this end, %ArmarX is organized in three layers: the *middleware layer* which provides core features such as communication methods and deployment concepts, the *robot framework layer*
  which consists of standardized components that can be tailored to customize the robot software framework, and the *application layer* where robot programs are implemented. 
  This layered architecture eases the development of robot software architectures by making use of standard interfaces and ready-to-use components which can be customized, 
  extended and/or parametrized according to the robot’s capabilities and application. 
  To support the development process %ArmarX provides a set of tools such as plug-in-based graphical user interfaces, a statechart editor, and tools for online inspection and state disclosure.
  ArmarX is being developed at the <a href="https://h2t.anthropomatik.kit.edu/" target="_blank" ><b>H2T</b></A> lab at the Karlsruhe Institute of Technology (KIT).

  \subsection armarx-main-getting-started Getting Started
    - \ref ArmarXDoc-Installation "Installation"
    - \ref ArmarXCore-QuickStart "Quick Start"
    - <b>Learn ArmarX</b>:
          - <a class="el" href="https://gitlab.com/ArmarX/meta/Academy">Visit the Academy</a>
          - Tutorials: <a class="el" href="https://gitlab.com/ArmarX/meta/Academy/-/tree/master/tutorials">in the Academy</a> | \ref ArmarXDoc-Tutorials "in the Documentation"
          - How To's: <a class="el" href="https://gitlab.com/ArmarX/meta/Academy/-/tree/master/how-tos">in the Academy</a> | \ref ArmarXDoc-HowTos "in the Documentation"
          - Code Examples: <a class="el" href="https://gitlab.com/ArmarX/meta/Academy/-/tree/master/examples">in the Academy</a>
          - Python Bindings: <a class="el" href="https://armarx.humanoids.kit.edu/python/">in the Documentation</a>
    - <b>Communication</b>:
          - <a class="el" href="https://www.lists.kit.edu/wws/info/armarx">ArmarX public mailing list</a>
          - <a class="el" href="https://gitlab.com/ArmarX/meta/Academy/-/issues">Q&A in the Academy</a>

  \subsection main-description-design-principles ArmarX Design Principles
    - Disclosure of Internal State
    - Extensive Support for Introspection and Debugging
    - Graphical editing of Control- and Dataflow
    - Interface Definition Language (IDL) for unified interfaces
    - Distributed Communication
    - Shared, Distributed Resources
    - Heterogenous Environments (C++, Java, Python, ...)
 

   \subsection armarx-main-description-structure Structure of ArmarX
    From robotics point of view ArmarX can be seen as 3 layers:
    
    \image html "images/Robot-Architecture.svg" "Layers of ArmarX"

  - **Low level**: Hardware drivers & \ref RobotAPI-SensorActorUnits "hardware abstraction"
  - **Mid level**: 
  	  - \ref Components "Robot Components" as services like \ref Component-RobotIK "inverse kinematics", \ref Component-MotionPlanningServer "motion planning", \ref RobotComponents-Tutorials-mmmtutorial "trajectory execution"
      - \ref MemoryX-Overview "Robot Memory" consisting of \ref MemoryX-PriorKnowledge "prior knowledge", \ref MemoryX-LongtermMemory "long-term" and \ref MemoryX-WorkingMemory "working memory"
   	  - \ref VisionX-Overview "Perception capabilities" like object recognition and self localization
  - **High level**: Coordination of low and mid level via \ref Statechart "statecharts" and symbolic planning



  The structural overview of %ArmarX from software point of view looks as follows:
  \image html "images/ArmarX_SW_Structure.svg"

  As illustrated above %ArmarX is organized in different layers:

  - <b> Middleware Layer </b>\n
  The Middleware Layer implements all facilities to realize distributed applications. It abstracts the communication mechanisms, provides basic building blocks of the
  distributed application, and provides entry points for visualization and debugging. With %ArmarX RT a bridge to real time components can be established,
  as it is needed for accessing low level robot control modules. \n
  The following projects are related to the %ArmarX Middleware:
        - \ref ArmarXCore-Overview "ArmarXCore"
        - \ref api-armar4 "ArmarXRT"

  - <b> Robot Framework Layer </b>\n
  The Robot Framework Layer comprises several projects which provide access to sensori-motor capabilities on a higher level.
  These projects include memory (\ref api-memoryx "MemoryX"), robot and world model, perception (\ref api-visionx "VisionX"), and execution modules.
  The basic building blocks of the Middleware Layer are specialized in the robot API for the different functionalities provided by the Robot Framework Layer.
  By means of extension and further specialization, a robot specific API can be implemented by the developer.
  Usually, such robot specific APIs include interface implementations for accessing the lower sensori-motor levels and models dedicated to the actual robot.
  Depending on the application, this layer can also include specialized memory structures, instances of perceptual functions, and control algorithms. \n
  The following projects are related to the %ArmarX Robot Framework Layer:
    - \ref RobotAPI-Overview "RobotAPI"
    - \ref VisionX-Overview "VisionX"
    - \ref MemoryX-Overview "MemoryX"


  - <b> Application Layer </b>\n
  In the Application Layer, the final robot program is implemented as a distributed application, making use of the generic and specific robot APIs. \n
  The following projects are related to the %ArmarX Application Layer:
    - \ref RobotSkillTemplates-Overview "Robot Skills"
    - \ref api-armar3 "Armar3"
    - \ref api-armar4 "Armar4"


  - <b>%ArmarX Toolset</b>\n
    The %ArmarX Toolset provides several tools to ease the development of robot applications.
    The toolset comprise graphical user interfaces, world and state chart editors and inspection tools.\n
    The following projects are related to the %ArmarX Toolset:
    - \ref ArmarXGui-Overview "ArmarXGui"
    - \ref ArmarXGui-GuiPlugins-StatechartEditorController "Statechart Editor"
    - \ref armarx-tools "ArmarX CLI Tools"
    - \ref ArmarXSimulation-Overview "Simulator"






<b>Main publications</b><br/>
- N. Vahrenkamp, M. Wächter, M. Kröhnert, K. Welke and T. Asfour, *The Robot Software Framework ArmarX*,  it - Information Technology. Vol. 57, No. 2, pp. 99 – 111, March 2015
[<a href="http://h2t.anthropomatik.kit.edu/pdf/Vahrenkamp2015.pdf" target="_blank">PDF</a>] [<a href="http://dx.doi.org/10.1515/itit-2014-1066" target="_blank">DOI</a>]
- M. Wächter, S. Ottenhaus, M. Kröhnert, N. Vahrenkamp and T. Asfour (2016), *The ArmarX Statechart Concept: Graphical Programming of Robot Behaviour*, Front. Robot. AI 3:33.
[<a href="http://h2t.anthropomatik.kit.edu/pdf/Waechter2016.pdf">PDF</a>] [<a href="http://journal.frontiersin.org/article/10.3389/frobt.2016.00033/full">DOI</a>]

<div style="display:none">
  - \subpage ArmarXDoc-Overview "Package Overviews"
  - \subpage ArmarXDoc-Installation "Installation"
  - \subpage ArmarXCore-QuickStart "QuickStart"
  - \subpage ArmarXDoc-Tutorials "Tutorials"
  - \subpage ArmarXDoc-HowTos "Howtos"
  - \subpage ArmarXDoc-FAQ "FAQ"
  - \subpage ArmarXDoc-Components "Components"
  - \subpage ArmarXDoc-GuiPlugins "Gui Plugins"
  - \subpage ArmarXCore-About "About"
  - \subpage ArmarXDoc-License "License"
</div>

*/
